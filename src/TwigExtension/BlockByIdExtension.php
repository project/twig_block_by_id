<?php

namespace Drupal\twig_block_by_id\TwigExtension;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\twig_tweak\View\BlockViewBuilder;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension implementing the drupal_block_by_id function.
 */
class BlockByIdExtension extends AbstractExtension {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\twig_tweak\View\BlockViewBuilder
   */
  protected $blockViewBuilder;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, BlockViewBuilder $block_view_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->blockViewBuilder = $block_view_builder;
  }

  /**
   * Generates a list of all Twig functions that this extension defines.
   *
   * @return array
   *   A key/value array that defines custom Twig functions. The key denotes the
   *   function name used in the tag, e.g.:
   *   @code
   *   {{ drupal_block_by_id() }}
   *   @endcode
   *
   *   The value is a standard PHP callback that defines what the function does.
   */
  public function getFunctions() {
    return [
      'drupal_block_by_id' => new TwigFunction('drupal_block_by_id', [$this, 'drupalBlockById']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'twig_block_by_id.block_by_id_extension';
  }

  /**
   * Builds the render array for the block with the specified id.
   *
   * @param $id
   *   The block id.
   *
   * @return array
   *   A renderable array representing the content of the block.
   */
  public function drupalBlockById($id, array $configuration = [], bool $wrapper = TRUE) {
    /** @var \Drupal\block\Entity\Block $block */
    $block = $this->entityTypeManager->getStorage('block')->load($id);
    $configuration = array_merge($block->get('settings'), $configuration);
    $configuration['id'] = $id;
    return $this->blockViewBuilder->build($block->getPluginId(), $configuration, $wrapper);
  }

}
