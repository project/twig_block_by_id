# Twig Block by id

This module simply provides a `drupal_block_by_id()` twig function to render a block in a theme, by configuration id.

## Example of use

`{{ drupal_block_by_id('my_block_id') }}`

## Features

Twig Tweak module provides a `drupal_block()` function, which requires the plugin configuration value to be identified, apart from the configuration id. When using custom blocks, this is not easy to identify, it's not in the UI, and it is a large alpha-numeric number, like 'block_content:e701a91b-419f-48fc-9b48-136138eaebec'. Apart, you would need to define all the configuration manually, as the second parameter, instead of using the configuration set in the block instance in Block Layout page.

To makes things easier, this module provides just a `drupal_block_by_id()` function that takes only the block instance id instead of the plugin id, and applies the block configuration by default, allowing to override those settings as second parameter. It uses Twig Tweak module's `drupal_block()` function internally.

Twig Tweak module also provides a `drupal_entity()` function that could be used with blocks. However the rendering process is different, and we have run into cases where it bypasses the cache contexts and the new content is not displayed where saving after editing the block in the UI, requiring a cache clear. Using `drupal_block_by_id()` you won't run into that problem.

## Requirements

This module requires [Twig Tweak](https://www.drupal.org/project/twig_tweak) module installed.

## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
